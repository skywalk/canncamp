# BigGAN-Tensorflow迁移到Ascend
Simple Tensorflow implementation of ["Large Scale GAN Training for High Fidelity Natural Image Synthesis" (BigGAN)](https://arxiv.org/abs/1809.11096)

![main](./assets/main.png)

## Issue
* **The paper** used `orthogonal initialization`, but `I used random normal initialization.` The reason is, when using the orthogonal initialization, it did not train properly.
* I have applied a hierarchical latent space, but **not** a class embeddedding.

## Usage
### dataset
* `mnist` and `cifar10` are used inside keras
* For `your dataset`, put images like this:

```
├── dataset
   └── YOUR_DATASET_NAME
       ├── xxx.jpg (name, format doesn't matter)
       ├── yyy.png
       └── ...
```
### train
* python main.py --phase train --dataset celebA-HQ --gan_type hinge

### test
* python main.py --phase test --dataset celebA-HQ --gan_type hinge

## Architecture
![architecture](./assets/architecture.png)

### 128x128
![128](./assets/128.png)

### 256x256
![256](./assets/256.png)

### 512x512
![512](./assets/512.png)

## Author
Junho Kim

# 迁移到Ascedn
main.py文件中加入：


```
from tensorflow.core.protobuf.rewriter_config_pb2 import RewriterConfig
from npu_bridge.npu_init import *
# Moxing库
import moxing as mox

```
程序体中间修改如下：
```
    # 将数据集从OBS拷贝到ModelArts
    mox.file.copy_parallel(src_url="obs://cann1/cat/cat00/", dst_url="dataset/cat")
    # 创建Profiling所需的目录
    os.mkdir("/cache/profiling")

    # open session
    config = tf.ConfigProto(allow_soft_placement=True)
    custom_op = config.graph_options.rewrite_options.custom_optimizers.add()
    custom_op.name = "NpuOptimizer"
    custom_op.parameter_map["use_off_line"].b = True  # 必须显示开启，在昇腾AI处理器执行训练
    config.graph_options.rewrite_options.remapping = RewriterConfig.OFF   # 必须显示关闭remap
    # config.graph_options.rewrite_options.optimizers.extend(["GradFusionOptimizer"])  # 分布式添加
    custom_op.parameter_map["profiling_mode"].b = True
    custom_op.parameter_map["profiling_options"].s = tf.compat.as_bytes(
        '{"output":"/cache/profiling","task_trace":"on","aicpu":"on"}')

    # https://gitee.com/ascend/modelzoo/wikis/Modelarts%E9%87%87%E9%9B%86Profiling%E6%80%A7%E8%83%BD%E6%95%B0%E6%8D%AE?sort_id=3646848
    # 参考以上链接

    

    with tf.Session(config=config) as sess:
        # default gan = BigGAN_128

        if args.img_size == 512 :
            gan = BigGAN_512(sess, args)
        elif args.img_size == 256 :
            gan = BigGAN_256(sess, args)
        else :
            gan = BigGAN_128(sess, args)

        # build graph
        gan.build_model()

        # show network architecture
        show_all_variables()

        if args.phase == 'train' :
            # launch the graph in a session
            gan.train()

            # visualize learned generator
            gan.visualize_results(args.epoch - 1)

            print(" [*] Training finished!")

        if args.phase == 'test' :
            gan.test()
            print(" [*] Test finished!")
    # 将profiling数据拷贝回OBS
    print(os.listdir("/cache/profiling"), os.listdir("/cache"))
    mox.file.copy_parallel(src_url="/cache/profiling", dst_url="obs://cann1/profiling/")
```

同时修改了utils.py文件，在其中加入
```
import imageio
def imsave(images, size, path):
    # image = np.squeeze(merge(images, size)) #
    # return scipy.misc.imsave(path, merge(images, size))
    return imageio.imwrite(path, merge(images, size))
```

使用pycharm的modelarts toolkit插件提交任务，
任务设定为：
boot文件：/Users/skywalk/github/huawei/BigGAN-Tensorflow/main.py

obs 目录 ：/cann1/
obs dataset目录：/cann1/cat 
我使用下来，obs dataset目录需要写 obs 中数据集的上一级目录。

提交，即可执行。我这里设定了1个epoch2个iteration，所以所需时间很少，只需要跑5分钟左右。

# 性能分析
按照上面的代码，执行的时候就会生成profiling文件，并在最终cp到obs上面。
从obs拿下来，再上传到专门的ai1s服务器上，执行分析。

为方便执行msprof.pyc脚本，您可以使用HwHiAiUser用户执行命令alias msprof='python3.7.5 /home/HwHiAiUser/Ascend/ascend-toolkit/latest/toolkit/tools/profiler/profiler_tool/analysis/msprof/msprof.pyc'设置别名，后续就可以不用进入那个路径了。

 执行如下三条命令，即可导出解析文件
```
 msprof import -dir JOBHIEFJGGBDBDCDAHDBEGBAAAAAAAAA/
 msprof export timeline -dir JOBHIEFJGGBDBDCDAHDBEGBAAAAAAAAA/
 msprof export summary  -dir JOBHIEFJGGBDBDCDAHDBEGBAAAAAAAAA/
```

将summary子目录文件下载到本机，使用excel打开，排序分析即可。

按照耗时排序，发现aigpu里面截断用的时间最多，op summary里面MatMul_1使用的时间最多，op statistack里面BatchMatMul 使用时间最多 。matmul就是计算两个tensor的乘积。